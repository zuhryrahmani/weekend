// main
import React, { useState, useEffect } from 'react';
import { makeStyles, useMediaQuery, Button } from '@material-ui/core';
import axios from 'axios';
import AOS from 'aos';
import 'aos/dist/aos.css';

// components
import Slider from './components/Slider';
import Card from './components/Card';

// assets
import headerImg from './assets/png/header.png';
import bg1 from './assets/svg/bg-1.svg';
import lego1 from './assets/png/lego-1.png';
import lego2 from './assets/png/lego-2.png';
import lego3 from './assets/png/lego-3.png';

const App = () => {

  const [testimonial, setTestimonial] = useState([]);
  const [helpTips, setHelpTips] = useState([]);

  const wUp1366 = useMediaQuery('(min-width:1366px)');
  const wDown953 = useMediaQuery('(max-width:953px)');
  const wDown640 = useMediaQuery('(max-width:640px)');
  const wDown619 = useMediaQuery('(max-width:619px)');
  const useStyles = makeStyles({
    header: {
      height: 67,
      backgroundColor: '#fff',
      position: 'absolute',
      top: 0,
      left: 0,
      right: 0,
      zIndex: 10
    },
    headerInfo: {
      margin: `auto 0 auto ${wDown953 ? 24 : 7}px`,
      display: 'flex',
      height: 33,
    },
    bodyTop: {
      backgroundColor: '#eebece',
      position: 'relative',
      overflow: 'hidden',
      paddingTop: 67,
      textAlign: 'center'
    },
    bg1: {
      position: 'absolute',
      top: 0,
      left: '50%',
      transform: 'translateX(-50%)',
      width: wUp1366 ? '100%' : 'initial'
    },
    container: {
      position: 'relative',
      zIndex: 1,
      maxWidth: 953,
      margin: '0 auto',
      height: '100%'
    },
    title: {
      color: '#fff',
      fontSize: wDown619 ? 52 : 62,
      fontWeight: 900,
      marginTop: 70
    },
    desc: {
      color: '#fff',
      fontSize: wDown619 ? 16 : 21,
      fontWeight: 600,
      fontStyle: 'italic',
      marginTop: 10
    },
    button: {
      backgroundColor: '#fff',
      color: '#000000',
      width: 231,
      height: 58,
      borderRadius: 29,
      textTransform: 'capitalize',
      fontSize: 16,
      fontFamily: 'Work Sans, sans-serif',
      boxShadow: '0 20px 30px 0 rgba(249, 131, 171, 0.5)',
      position: 'absolute',
      bottom: 50,
      left: '50%',
      transform: 'translateX(-50%)',
      '& .MuiTouchRipple-root': {
        color: '#eebece',
      },
      '&:hover': {
        boxShadow: '0 20px 30px 0 rgba(249, 131, 171, 0.5)',
        backgroundColor: 'rgb(250, 244, 244)'
      }
    },
    def1: {
      textAlign: 'right',
      fontSize: 21,
      margin: `${wDown619 ? '241px' : '300px'} auto 0`,
      width: wDown640 ? '100%' : 619,
      fontWeight: 500,
      padding: wDown640 ? '0 32px' : 0,
      boxSizing: 'border-box'
    },
    def2: {
      fontSize: 21,
      fontWeight: 600,
      fontStyle: 'italic',
      color: '#fff',
      width: wDown640 ? '100%' : 619,
      textAlign: 'right',
      margin: `${wDown619 ? '20px' : '30px'} auto 0`,
      padding: wDown619 ? '0 32px 220px' : wDown640 ? '0 32px 197px' : '0 0 197px',
      boxSizing: 'border-box'
    },
    lego2: {
      position: 'absolute',
      top: wDown619 ? 667 : 768,
      right: 0
    },
    dot: {
      position: 'absolute',
      left: 77,
      bottom: wDown619 ? 93 : 70,
      width: 89,
      height: 89,
      backgroundColor: '#0b24fb',
      borderRadius: '50%'
    },
    bodyBottom: {
      color: '#fff',
      textAlign: 'center',
      position: 'relative',
      zIndex: 1,
      marginTop: -113,
      paddingBottom: wDown953 ? 404 : 203
    },
    subtitle: {
      fontSize: 32,
      fontWeight: 900,
      marginBottom: 30,
      textAlign: wDown619 ? 'left' : 'inherit',
      paddingLeft: wDown619 ? 32 : 0
    },
    testi: {
      marginBottom: wDown619 ? 67 : 96,
      display: 'flex',
      justifyContent: 'center'
    },
    info: {
      fontSize: wDown619 ? 16 : 18,
      fontWeight: 'normal',
      width: wDown619 ? '100%' : 619,
      margin: '0 auto',
      padding: `0 ${wDown619 ? '32px' : 0}`,
      boxSizing: 'border-box',
      textAlign: wDown619 ? 'left' : 'center'
    },
    help: {
      marginBottom: wDown619 ? 67 : 127,
      display: 'flex',
      flexDirection: wDown953 ? 'column' : 'row',
      justifyContent: 'center',
    },
    lego3: {
      position: 'absolute',
      left: 0,
      bottom: 0,
      width: !wDown619 ? 244 : 253,
      height: !wDown619 ? 325 : 337,
      objectFit: 'cover',
      objectPosition: 'center'
    },
    footer: {
      backgroundColor: '#0b24fb',
      height: 67,
      color: '#fff'
    },
    footerTag: {
      border: '1px solid #fff',
      borderRadius: 12,
      width: 126,
      height: 24,
      display: 'inline-block',
      lineHeight: '24px',
      fontSize: 14,
      fontWeight: 300,
      textAlign: 'center'
    }
  });
  const classes = useStyles();

  useEffect(() => {
    // get testimonial
    axios.get('https://wknd-take-home-challenge-api.herokuapp.com/testimonial')
      .then(res => {
        setTestimonial(res.data);
      })
      .catch(err => alert(`Error: ${err}`));
    // get help & tips 
    axios.get('https://wknd-take-home-challenge-api.herokuapp.com/help-tips')
      .then(res => {
        setHelpTips(res.data);
      })
      .catch(err => alert(`Error: ${err}`));
    // animation
    AOS.init({duration:800});
  }, []);

  return (
    <div style={{overflow:'hidden'}}>
      <div className={classes.header}>
        <div className={classes.container} style={{display:'flex'}}>
          <div className={classes.headerInfo}>
            <div style={{marginRight:13}}>
              <img src={headerImg} />
            </div>
            <div>
              <p style={{fontSize:12, fontWeight:'normal'}}>Good Morning</p>
              <p style={{fontSize:16, fontWeight:'bold'}}>Fellas</p>
            </div>
          </div>
        </div>
      </div>
      <div className={classes.bodyTop}>
        <img src={bg1} className={classes.bg1} />
        <img src={lego2} className={classes.lego2} />
        <div className={classes.container}>
          <h1 className={classes.title}>WEEKEND FROM HOME</h1>
          <p className={classes.desc}>Stay active with a little workout.</p>
          <div style={{marginTop:67, position:'relative'}} data-aos='zoom-in'>
            <img src={lego1} />
            <Button variant='contained' className={classes.button} >Let’s Go</Button>
          </div>
          <div data-aos='fade-right'>
            <div className={classes.def1}><span style={{color:'#0b24fb'}}>Definition;</span> a practice or exercise to test or improve one's fitness for athletic competition, ability, or performance to exhaust (something, such as a mine) by working to devise, arrange, or achieve by resolving difficulties. Merriam-Webster.com Dictionary.</div>
            <div className={classes.def2}>-weekend team</div>
          </div>
          <div className={classes.dot} />
        </div>
      </div>
      <div className={classes.bodyBottom}>
        <div style={{position:'relative', zIndex:1}}>
          <div className={classes.subtitle} style={{textShadow:'0 20px 30px rgba(249, 131, 171, 0.7)'}} data-aos='zoom-in'>Testimonial</div>
          <div className={classes.testi} data-aos='flip-down'>
            <Slider data={testimonial} />
          </div>
          <div className={classes.subtitle} data-aos='zoom-in'>POV</div>
          <div className={classes.info} style={{marginBottom:67}} data-aos='zoom-out'>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud ullamco laboris nisi ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</div>
          <div className={classes.subtitle} data-aos='zoom-in'>Resource</div>
          <div className={classes.info} style={{marginBottom: wDown619 ? 67 : 100}} data-aos='zoom-out'>These cases are perfectly simple and easy to distinguish. In a free hour, when our power of choice is untrammelled and when nothing prevents our being able to do what we like best</div>
          <div className={classes.subtitle} data-aos='zoom-in'>Help & Tips</div>
          <div className={classes.help} data-aos={!wDown953 && 'flip-down'}>
            {helpTips.map((item,i) => (
              <Card
                data={item}
                style={i===0 && !wDown953 ? {marginLeft:0} : i===helpTips.length-1 && !wDown953 ? {marginRight:0} : null}
                margin={wDown953 ? `${i===0 ? 0 : '10px'} auto 0` : '0 5px'}
                aos={wDown953 && 'flip-down'}
              />
            ))}
          </div>
          <div className={classes.subtitle} data-aos='zoom-in'>You’re all set.</div>
          <div className={classes.info} data-aos='zoom-out'>The wise man therefore always holds in these matters to this principle of selection.</div>
        </div>
        <img src={lego3} className={classes.lego3} />
      </div>
      <div className={classes.footer}>
        <div className={classes.container} style={{display:'flex', alignItems:'center', padding:`0 ${wDown953 ? '24px' : '7px'}`}}>
          <div style={{width:'50%', fontWeight:'bold'}}>
            wknd@<span style={{fontWeight:'300'}}>2020</span>
          </div>
          <div style={{width:'50%', textAlign:'right'}}>
            <span className={classes.footerTag}>alpha version 0.1</span>
          </div>
        </div>
      </div>
    </div>
  );
}

export default App;

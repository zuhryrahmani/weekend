// main
import React from 'react';
import { makeStyles } from '@material-ui/core';

// components
import IconButton from '../IconButton';

const Card = ({data, style, margin='0 5px', aos}) => {
  
  const useStyles = makeStyles({
    card: {
      width: 311,
      height: 176,
      margin: margin,
      background: `url(${data.image}) no-repeat`,
      backgroundSize: 'cover',
      backgroundPosition: 'center',
      position: 'relative',
    },
    cardInfo: {
      position: 'absolute',
      bottom: 0,
      left: 0,
      right: 0,
      height: 72,
      backgroundColor: 'rgba(0,0,0,0.5)',
      padding: '17px 25px',
      boxSizing: 'border-box'
    },
    info: {
      fontWeight: 'bold',
      textAlign: 'left',
      width: 218
    },
    button: {
      position: 'absolute',
      right: 20,
      top: 20
    }
  });
  const classes = useStyles();

  return(
    <div className={classes.card} style={style} data-aos={aos}>
      <div className={classes.cardInfo}>
        <div className={classes.info}>{data.title}</div>
        <div className={classes.button}><IconButton /></div>
      </div>
    </div>
  );
};

export default Card;
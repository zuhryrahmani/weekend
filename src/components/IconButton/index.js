// main
import React from 'react';
import { makeStyles, IconButton } from '@material-ui/core';
import { ArrowForwardRounded, ArrowBackRounded } from '@material-ui/icons';

const ArrowButton = ({type='right', style, disabled, id, onClick}) => {
  
  const useStyles = makeStyles({
    button: {
      padding: 0,
      height: 32,
      width: 32,
      backgroundColor: '#fff',
      '&:hover': {
        backgroundColor: 'rgb(237, 236, 247)',
      },
      '&.MuiIconButton-root.Mui-disabled': {
        backgroundColor: '#fff',
        opacity: 0.35
      }
    }
  });
  const classes = useStyles();

  return(
    <IconButton
      id={id}
      className={classes.button}
      style={style}
      disabled={disabled}
      onClick={onClick}
    >
      {type==='right' ? <ArrowForwardRounded style={{color:'#0b24fb', fontSize:'1rem'}}/> : <ArrowBackRounded style={{color:'#0b24fb', fontSize:'1rem'}}/>}
    </IconButton>
  );
};

export default ArrowButton;
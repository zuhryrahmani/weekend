// main
import React, { useState, useRef } from 'react';
import { makeStyles, useMediaQuery } from '@material-ui/core';

// components
import IconButton from '../IconButton';

const Slider = ({data=[]}) => {
  
  const wDown750 = useMediaQuery('(max-width:750px)');
  const wDown640 = useMediaQuery('(max-width:640px)');
  const useStyles = makeStyles({
    slider: {
      display: 'flex',
      alignItems: 'center',
      width: wDown640 && '100%'
    },
    scroll: {
      width: wDown640 ? '100%' : 596,
      height: 140,
      overflow: 'auto',
      scrollbarWidth: 'none',
      scrollBehavior: 'smooth',
      '&::-webkit-scrollbar': {
        display: 'none'
      }
    },
    container: {
      display: 'flex',
      width: 'fit-content'
    },
    box: {
      width: 247,
      height: 140,
      backgroundColor: '#fff',
      boxShadow: '0 -10px 20px 0 rgba(249, 131, 171, 0.2)',
      padding: 20,
      color: '#000',
      textAlign: 'left',
      boxSizing: 'border-box',
    }
  });
  const classes = useStyles();

  const ref = useRef(null);
  const [scroll, setScroll] = useState(0);

  const handleScroll = () => {
    setScroll(ref.current.scrollLeft);
  };

  const handlePrev = () => {
    ref.current.scrollLeft = scroll-514 >= 0 ? scroll-514 : 0;
  };

  const handleNext = () => {
    ref.current.scrollLeft = scroll + 514;
  };


  return(
    <div className={classes.slider}>
      {!wDown750 && (
        <IconButton
          type='left'
          style={{marginRight:25}}
          onClick={handlePrev}
          disabled={scroll === 0 ? true : false}
        />
      )}
      <div className={classes.scroll} ref={ref} onScroll={handleScroll}>
        <div className={classes.container}>
          {data.map((item,i) => (
            <div className={classes.box} style={{margin: `0 ${i===data.length-1 && wDown640 ? '32px' : 0} 0 ${i===0 ? wDown640 ? '32px' : 0 : '10px' }`}}>
              <h1 style={{fontSize:32, fontWeight:900}}>{item.by}</h1>
              <p style={{fontSize:12, marginTop:10}}>{item.testimony}</p>
            </div>
          ))}
        </div>
      </div>
      {!wDown750 && (
        <IconButton
          style={{marginLeft:25}}
          onClick={handleNext}
          disabled={data.length > 0 ? scroll >= ((data.length*247) + ((data.length-1)*10) - 596) ? true : false : false}
        />
      )}
    </div>
  );
};

export default Slider;